package com.atguigu.test;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.TreeSet;


class person implements Comparable{
    private int id;
    private String name;

    public person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public person() {
    }

    @Override
    public String toString() {
        return "person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if(o==this)
            return true;
        if(o instanceof person)
        {
            person a= (person) o;
            return this.name.equals(a.name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof person)
        {
         person a= (person) o;
         return a.id-this.id;
        }
        return 0;
    }
}

public class SimpleDateFormat1
{

    public void test(int i) throws Exception {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("Gyyyy-MM-dd(hh:mm:ss)");
    Date date = new Date();
    String format = simpleDateFormat.format(date);
    System.out.println(format);
        TreeSet<person> treeSet = new TreeSet<person>();
        treeSet.add(new person(1,"zhujunjie"));
        treeSet.add(new person(3,"zhjunjie"));
        treeSet.add(new person(2,"124"));
        System.out.println(treeSet);
        if(i==0)
        throw new Exception("不能为3");
    }
    @Test
    public void test2()
    {
        try {
            this.test(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
