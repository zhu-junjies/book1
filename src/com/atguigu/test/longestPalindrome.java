package com.atguigu.test;

import org.junit.Test;

public class longestPalindrome {
    public String longestPalindrome(String s) {
        int len;
        int i,j;
        int max=0,place=0;
        boolean s1[][] = new boolean[s.length()][s.length()];
        char[] chars = s.toCharArray();
        for (i=0;i<s.length();i++)
        {
            s1[i][i]=true;
            max=1;
        }
        for(len=2;len<=s.length();len++)
        {
            for (i=0;i<s.length();i++)
            {
                j=i+len-1;
                if(j>=s.length())
                    break;
                if(chars[j]==chars[i])
                {
                    if(len<=3)
                    {
                        s1[i][j]=true;
                    }
                    else if(s1[i+1][j-1]==true)
                    {
                        s1[i][j]=true;
                    }
                }
                else
                    s1[i][j]=false;
                if(s1[i][j]&&j-i+1>max)
                {
                    place=i;
                    max=j-i+1;
                }
            }
        }
        return s.substring(place,place+max);
    }
    @Test
    public void test()
    {
        String abcabba = this.longestPalindrome("aba");
        System.out.println(abcabba);
    }
}
