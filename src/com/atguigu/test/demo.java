package com.atguigu.test;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class demo {
        public int lengthOfLongestSubstring(String s) {
            Set<Character> set = new HashSet<>();
            int i,ans=0,right=-1;
            int leng=s.length();
            for (i=0;i<leng;i++)
            {
                while (i!=0)
                {
                    set.remove(s.charAt(i-1));
                }
                while (right+1<leng&&!set.contains(s.charAt(right+1)))
                {
                    set.add(s.charAt(right+1));
                    right++;
                }
                ans=Math.max(right-i,ans);
            }
            return ans;
        }
        @Test
    public void test()
    {
        System.out.println(this.lengthOfLongestSubstring(""));
    }
}
