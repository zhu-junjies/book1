package com.atguigu.test;

import com.atguigu.dao.BookDao;
import com.atguigu.dao.impl.BookDaoImpl;
import com.atguigu.dao.impl.PageDaoImpl;
import com.atguigu.pojo.Book;
import com.atguigu.pojo.Page;
import com.atguigu.service.BookService;
import com.atguigu.service.impl.BookServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class BookDaoTest1 {

    BookDao userDao = new BookDaoImpl();

    @Test
    public void addBook() {
        userDao.addBook(new Book(null,"zhujunjie","朱俊杰",new BigDecimal(999),11,11,"123"));
    }

    @Test
    public void deleteBookById() {
        userDao.deleteBookById(20);
    }

    @Test
    public void updateBook() {
        userDao.updateBook(new Book(22,"zhujie","朱俊杰",new BigDecimal(999),11,11,"3"));
    }

    @Test
    public void queryBookById() {
        System.out.println(userDao.queryBookById(14));
    }

    @Test
    public void queryBooks() {
        PageDaoImpl pageDao = new PageDaoImpl();
        BookService bookService = new BookServiceImpl();
        Page bookPage = bookService.getBookByPricePage(4,1,1000,10);
        List<Book> t_book = pageDao.getItemByPricePage(Book.class, "t_book", 1, 4,1000,10);
        System.out.println(t_book);
        System.out.println(bookPage.getPageItem());
    }
}