package com.atguigu.web;

import com.atguigu.pojo.Page;
import com.atguigu.service.BookService;
import com.atguigu.service.impl.BookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientByPriceServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String max = req.getParameter("max");
        String min = req.getParameter("min");
        int maxx=max==""||max==null?Integer.MAX_VALUE:Integer.parseInt(max);
        int minn=min==""||min==null?0:Integer.parseInt(min);
        BookService bookService = new BookServiceImpl();
        req.setAttribute("max",max);
        req.setAttribute("min",min);
        String pageNo = req.getParameter("pageNo");
        StringBuilder url=new StringBuilder("client/Price?");
        if(max!=null&&!"".equals(max)) {
            url.append("max=" + max + "&");
        }if(min!=null&&!"".equals(min)) {
            url.append("min=" + min);
        }
        if(pageNo==null) {
            pageNo="1";
        }
        Page bookPage = bookService.getBookByPricePage(4,Integer.parseInt(pageNo),maxx,minn);
        bookPage.setUrl(url.toString());
        req.setAttribute("page",bookPage);
        req.getRequestDispatcher("/pages/cient/index.jsp").forward(req,resp);
    }
}
