package com.atguigu.web;

import com.atguigu.pojo.Book;
import com.atguigu.pojo.Cart;
import com.atguigu.pojo.CartItem;
import com.atguigu.service.BookService;
import com.atguigu.service.impl.BookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class CartServlet extends BaseServlet {
    BookService bookService = new BookServiceImpl();
protected void updateCount(HttpServletRequest req,HttpServletResponse resp) throws IOException {
    String count = req.getParameter("count");

    String bookId = req.getParameter("bookId");

    HttpSession session = req.getSession();

    Cart cart = (Cart) session.getAttribute("cart");

    cart.updateCount(Integer.valueOf(bookId),Integer.valueOf(count));

    session.setAttribute("cart",cart);

    resp.sendRedirect(req.getHeader("Referer"));
}
protected void deleteItem(HttpServletRequest req,HttpServletResponse resp) throws IOException {
    String id = req.getParameter("bookId");

    Cart cart = (Cart)req.getSession().getAttribute("cart");

    cart.deleteItem(Integer.valueOf(id));

    req.getSession().setAttribute("cart",cart);

    resp.sendRedirect(req.getHeader("Referer"));
}
protected void clear(HttpServletRequest req,HttpServletResponse resp) throws IOException {
    Cart cart = (Cart) req.getSession().getAttribute("cart");

    cart.clear();

    resp.sendRedirect(req.getHeader("Referer"));
}
protected void addItem(HttpServletRequest req,HttpServletResponse resp) throws IOException {

    HttpSession session = req.getSession();

    String id = req.getParameter("bookId");

    Book book = bookService.queryBookById(Integer.valueOf(id));

    CartItem cartItem = new CartItem(Integer.valueOf(id), book.getName(), 1, book.getPrice(), book.getPrice());

    Cart cart = (Cart)session.getAttribute("cart");

    if(cart==null)
    {
        session.setAttribute("cart",new Cart());
        cart= (Cart)session.getAttribute("cart");
    }

    session.setAttribute("bookName",book.getName());

    cart.addItem(cartItem);

    resp.sendRedirect(req.getHeader("Referer"));

}
}
