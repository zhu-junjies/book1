package com.atguigu.web;
import com.atguigu.pojo.User;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atguigu.service.UserService;
import com.atguigu.service.impl.UserServiceImpl;
import com.atguigu.utils.WebUtils;

import static com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY;

public class UserServlet extends BaseServlet {
    UserService userService = new UserServiceImpl();
    protected void login(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = WebUtils.copyParamToBean(User.class, req.getParameterMap());
        User loginUser = userService.userLogin(user);
        if(loginUser==null)
        {
            System.out.println("用户名不存在");
            req.setAttribute("msg","用户名不存在或密码错误");
            req.setAttribute("username",user.getUsername());
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req,resp);
        }
        else
        {
            Cookie cookie = new Cookie("username",user.getUsername());
            cookie.setMaxAge(60*60*24*7);
            HttpSession session = req.getSession();
            session.setAttribute("user",user);
            cookie.setPath(req.getContextPath());
            System.out.println(req.getContextPath()+"/pages/user/login.jsp");
            resp.addCookie(cookie);
            req.getRequestDispatcher("/pages/user/login_success.jsp").forward(req,resp);
        }
    }
    protected void regist(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        User user = WebUtils.copyParamToBean( User.class, req.getParameterMap());
        String username=user.getUsername();
        String code = req.getParameter("code");
        ServletContext servletContext = getServletContext();
        HttpSession session = req.getSession();
        Object attribute = session.getAttribute(KAPTCHA_SESSION_KEY);
        if(code.equalsIgnoreCase((String) attribute))
        {
            if(userService.exitsUsername(username))
            {
                System.out.println("用户名已经存在");
                req.setAttribute("msg","用户名已经存在");
                req.setAttribute("username",username);
                req.getRequestDispatcher("/pages/user/regist.jsp").forward(req,resp);
            }
            else {
                userService.registUser(user);
                req.getSession().setAttribute(KAPTCHA_SESSION_KEY,null);
                req.getSession().setAttribute("user",user);
                req.getRequestDispatcher("/pages/user/regist_success.jsp").forward(req,resp);
            }
        }
        else {
            System.out.println("验证码错误");
            req.setAttribute("msg","验证码错误");
            req.setAttribute("username",username);
            req.setAttribute("code",code);
            req.getRequestDispatcher("/pages/user/regist.jsp").forward(req,resp);
        }
    }
    protected void loginOut(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        HttpSession session = req.getSession();
        session.setAttribute("user",null);
        resp.sendRedirect(req.getContextPath()+"/client/clientServlet");
    }
}
