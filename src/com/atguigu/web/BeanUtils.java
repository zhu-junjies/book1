package com.atguigu.web;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

public  class BeanUtils{

    public static <T> T Construct(T object, Map map){
        Class<?> aClass = object.getClass();
        Set set = map.keySet();
        for (Object field:set) {
            Field declaredField = null;
            try {
                declaredField = aClass.getDeclaredField((String) field);
                declaredField.setAccessible(true);
                declaredField.set(object,map.get(field));
            } catch (Exception e) {
            }
        }
        return object;
    }
}
