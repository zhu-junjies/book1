package com.atguigu.web;
import com.atguigu.pojo.Book;
import com.atguigu.pojo.Page;
import com.atguigu.service.BookService;
import com.atguigu.service.impl.BookServiceImpl;
import com.atguigu.utils.WebUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.desktop.UserSessionEvent;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class BookServlet extends BaseServlet{

    BookService bookService = new BookServiceImpl();

    protected void list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Book> books = bookService.queryBooks();

        req.setAttribute("books",books);

        req.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(req,resp);

    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        Map<String, String[]> parameterMap = req.getParameterMap();

        System.out.println(parameterMap);

        Book book1 = WebUtils.copyParamToBean(Book.class, parameterMap);

        bookService.updateBook(book1);

        String pageNo = req.getParameter("pageNo");

        String pageSize = req.getParameter("pageSize");

        resp.sendRedirect("/book2/manager/bookServlet?action=page&pageNo="+pageNo+"&pageSize="+pageSize);
    }
    protected void getBook(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

        String id = req.getParameter("id");

        Integer integer = Integer.valueOf(id);

        Book book = bookService.queryBookById(integer);

        String pageNo = req.getParameter("pageNo");

        String pageSize = req.getParameter("pageSize");

        req.setAttribute("pageSize",pageSize);

        req.setAttribute("pageNo",pageNo);

        req.setAttribute("book",book);

        req.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(req,resp);
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");

        Integer integer = Integer.valueOf(id);

        String pageNo = req.getParameter("pageNo");

        String pageSize = req.getParameter("pageSize");

        bookService.deleteBookById(integer);

        resp.sendRedirect("/book2/manager/bookServlet?action=page&pageNo="+pageNo+"&pageSize="+pageSize);

    }

    protected void addBook(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        Map<String, String[]> parameterMap = req.getParameterMap();

        String pageNo = req.getParameter("pageNo");

        String pageSize = req.getParameter("pageSize");

        Book book = WebUtils.copyParamToBean(Book.class, parameterMap);

        bookService.addBook(book);

        resp.sendRedirect("/book2/manager/bookServlet?action=page&pageNo="+pageNo+"&pageSize="+pageSize);
    }
    protected void page1(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pageSize1 = req.getParameter("pageSize");

        String pageNo1 = req.getParameter("pageNo");

        if(pageNo1==null)
        {
            pageNo1="1";
        }
        if (pageSize1==null)
        {
            pageSize1="4";
        }

        long l = bookService.queryBookCounts();

        int pageSize = Integer.parseInt(pageSize1);

        long pageCount = l%pageSize==0?(l/pageSize):(l/pageSize+1);

        int pageNo = Integer.valueOf(pageNo1);

        if (pageNo>pageCount)
        {
            pageNo= (int) pageCount;
        }
        if (pageNo<=0)
        {
            pageNo=1;
        }

        List<Book> books = bookService.queryPage((pageNo-1)*pageSize, pageSize);

        req.setAttribute("books",books);

        req.setAttribute("count",l);

        req.setAttribute("pageSize",pageSize);

        req.setAttribute("pageNo",pageNo);

        req.setAttribute("start",pageNo*pageSize);

        req.setAttribute("pageCount",pageCount);

        req.getRequestDispatcher("/pages/manager/book_manager_page.jsp").forward(req,resp);

    }
    protected void page(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pageSize1 = req.getParameter("pageSize");

        String pageNo1 = req.getParameter("pageNo");

        if(pageNo1==null)
        {
            pageNo1="1";
        }
        if (pageSize1==null)
        {
            pageSize1="4";
        }

        Page bookPage = bookService.getBookPage(Integer.parseInt(pageSize1), Integer.parseInt(pageNo1));

        bookPage.setUrl("manager/bookServlet?action=page");

        req.setAttribute("page",bookPage);

        req.getRequestDispatcher("/pages/manager/book_manager_page.jsp").forward(req,resp);

    }
}
