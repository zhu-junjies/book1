package com.atguigu.file;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Base64;

public class Download extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String downLoadFile="加密演示.jpg";

        ServletContext servletContext = getServletContext();

        String mimeType = servletContext.getMimeType("/File/123/加密演示.jpg");

        resp.setContentType(mimeType);

        InputStream resourceAsStream = servletContext.getResourceAsStream("/File/123/加密演示.jpg");

        ServletOutputStream outputStream = resp.getOutputStream();

        if(req.getHeader("User-Agent").contains("FireFox"))
        {
            Base64.Encoder encoder = Base64.getEncoder();
            resp.setHeader("Content-Disposition","attachment;fileName=?UTF-8?B?"+encoder.encode("中国".getBytes())+"?=");
        }
        else
        {
            resp.setHeader("Content-Disposition","attachment;fileName="+ URLEncoder.encode("中国.jpg","UTF-8"));
        }

        IOUtils.copy(resourceAsStream,outputStream);

    }

}
