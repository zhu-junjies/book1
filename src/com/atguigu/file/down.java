package com.atguigu.file;
import org.apache.commons.io.IOUtils;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
public class down extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext servletContext=getServletContext();
        String mimeType = servletContext.getMimeType("123");
        resp.setContentType(mimeType);
        InputStream resourceAsStream = servletContext.getResourceAsStream("123");
        resp.setHeader("Content-Disposition","attachment:filename"+URLEncoder.encode("123","UTF-8"));
        ServletOutputStream outputStream = resp.getOutputStream();
        IOUtils.copy(resourceAsStream,outputStream);
    }
}
