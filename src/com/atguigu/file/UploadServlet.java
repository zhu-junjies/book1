package com.atguigu.file;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class UploadServlet extends HttpServlet {

    /**
     * 用来处理文件上传
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(ServletFileUpload.isMultipartContent(req))

        {   //创建
            FileItemFactory fileItemFactory = new DiskFileItemFactory();
            ServletFileUpload servletFileUpload = new ServletFileUpload(fileItemFactory);
            try {//解析上传的数据得到每一个FileItem的值
                List<FileItem> list = servletFileUpload.parseRequest(req);
                for (FileItem fileItem:list) {
                    if(fileItem.isFormField())
                    {
                        System.out.println("表单项的name属性值"+fileItem.getFieldName());
                        System.out.println("表单项的value属性值"+fileItem.getString("UTF-8"));
                        req.setAttribute("name",fileItem.getString("UTF-8"));
                    }
                    else
                    {
                        System.out.println("表单项的name属性值"+fileItem.getFieldName());
                        System.out.println("上传的文件名字"+fileItem.getName());
                        fileItem.write(new File("D://"+fileItem.getName()));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
