package com.atguigu.pojo;

import java.awt.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;

public class Cart {

    private BigDecimal totalPrice;
    private Map<Integer,CartItem> items=new LinkedHashMap<Integer,CartItem>();

    public void addItem(CartItem cartItem)
    {
        CartItem cartItem1 = items.get(cartItem.getId());
        if (cartItem1==null)
        {
            items.put(cartItem.getId(),cartItem);
        }
        else
        {
            cartItem1.setCount(cartItem1.getCount()+cartItem.getCount());//数量累加
            cartItem1.setTotalPrice(cartItem1.getPrice().multiply(new BigDecimal(cartItem1.getCount())));//更新总金额

        }

    }
    public void deleteItem(Integer Id)
    {
        this.items.remove(Id);
    }
    public void clear()
    {
        this.items.clear();
    }
    public void updateCount(Integer Id,Integer count)
    {
        CartItem Item = this.items.get(Id);
        if (Item!=null)
        {
            Item.setCount(count);
            Item.setTotalPrice(Item.getPrice().multiply(new BigDecimal(Item.getCount())));
        }
    }
    public Cart(BigDecimal totalPrice ,Map<Integer,CartItem> items) {
        this.totalPrice = totalPrice;
        this.items = items;
    }

    public Cart() {
    }

    @Override
    public String toString() {
        return "Cart{" +
                "totalCount=" + getTotalCount() +
                ",totalPrice=" + getTotalPrice() +
                ", items=" + items +
                '}';
    }

    public BigDecimal getTotalPrice() {
     BigDecimal bigDecimal = new BigDecimal(0);
        for (Map.Entry<Integer, CartItem> Entry : items.entrySet()) {
            bigDecimal=bigDecimal.add(Entry.getValue().getTotalPrice());
        }
        return bigDecimal;
    }

    public Integer getTotalCount() {
        Integer totalCount=0;
        for (Map.Entry<Integer, CartItem> Entry : items.entrySet()) {
            totalCount+=Entry.getValue().getCount();
        }
        return totalCount;
    }

    public Map<Integer,CartItem>  getItems() {
        return items;
    }

    public void setItems(Map<Integer,CartItem> items) {
        this.items = items;
    }



}
