package com.atguigu.pojo;

import java.util.List;

public class Page<T> {
    private int pageNo;
    private int pageSize;
    private long pageCount;
    private List<T> pageItem;
    private int start;
    private long count;
    private String Url;

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", pageCount=" + pageCount +
                ", pageItem=" + pageItem +
                ", start=" + start +
                ", count=" + count +
                ", Url='" + Url + '\'' +
                '}';
    }

    public Page() {
    }

    public Page(int pageNo, int pageSize, long pageCount, List<T> pageItem, int start, long count) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
        this.pageItem = pageItem;
        this.start = start;
        this.count = count;
    }

    public int getPageNo() {
        return pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public long getPageCount() {
        return pageCount;
    }

    public List<T> getPageItem() {
        return pageItem;
    }

    public int getStart() {
        return start;
    }

    public long getCount() {
        return count;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public void setPageItem(List<T> pageItem) {
        this.pageItem = pageItem;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
