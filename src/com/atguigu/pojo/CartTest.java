package com.atguigu.pojo;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class CartTest {

    @Test
    public void addItem() {
        Cart cart= new Cart();
        cart.addItem(new CartItem(1,"s",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(21,"ms",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(12,"ms",54,new BigDecimal(8),new BigDecimal(54*8)));
        System.out.println(cart);
    }

    @Test
    public void deleteItem() {
        Cart cart= new Cart();
        cart.addItem(new CartItem(1,"s",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(21,"ms",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(12,"ms",54,new BigDecimal(8),new BigDecimal(54*8)));
        System.out.println(cart);
        cart.deleteItem(1);
        System.out.println(cart);
    }

    @Test
    public void clear() {
        Cart cart= new Cart();
        cart.addItem(new CartItem(1,"s",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(21,"ms",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(12,"ms",54,new BigDecimal(8),new BigDecimal(54*8)));
        System.out.println(cart);
        cart.clear();
        System.out.println(cart);
    }

    @Test
    public void updateCount() {
        Cart cart= new Cart();
        cart.addItem(new CartItem(1,"s",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(21,"ms",54,new BigDecimal(85),new BigDecimal(54*85)));
        cart.addItem(new CartItem(12,"ms",54,new BigDecimal(8),new BigDecimal(54*8)));
        Map<Integer, CartItem> items = cart.getItems();
        Collection<CartItem> values = items.values();
        System.out.println(cart);
        cart.updateCount(21,53);
        System.out.println(cart);
    }
}