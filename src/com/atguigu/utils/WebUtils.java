package com.atguigu.utils;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;


public class WebUtils {
    /**
     * 将集合中的数据注入到对象当中
     * @param clasz 注入的对象
     * @param map 要被注入的集合
     * @param <T> 你对象的类型
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static <T>T  copyParamToBean(Class<T> clasz, Map map) throws Exception {
        T t = clasz.getConstructor().newInstance();
        BeanUtils.populate(t,map);
        return t;
    }
}
