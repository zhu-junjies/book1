package com.atguigu.utils;

import java.io.*;

public class FileUtils {
    public static void copy(FileInputStream fileInputStream, FileOutputStream fileOutputStream) throws IOException {
        byte[]buffer= new byte[1024];
        int len;
        while ((len=fileInputStream.read(buffer))!=-1)
        {
            fileOutputStream.write(buffer,0,len);
        }
    }
    public static void copy(BufferedInputStream fileInputStream, BufferedOutputStream fileOutputStream) throws IOException {
        byte[]buffer= new byte[1024];
        int len;
        while ((len=fileInputStream.read(buffer))!=-1)
        {
            fileOutputStream.write(buffer,0,len);
        }
    }
}
