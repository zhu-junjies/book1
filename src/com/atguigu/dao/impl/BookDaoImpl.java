package com.atguigu.dao.impl;

import com.atguigu.dao.BookDao;
import com.atguigu.pojo.Book;

import java.util.List;

public class BookDaoImpl implements BookDao {
    BaseDao baseDao = new BaseDao();
    @Override
    public int addBook(Book book) {
        String sql="insert into `t_book`(`name`,`price`,`author`,`sales`,`stock`,`img_path`) values(?,?,?,?,?,?)";
        return baseDao.update(sql,book.getName(),book.getPrice(),book.getAuthor(),book.getSales(),book.getStock(),book.getImgPath());
    }

    @Override
    public int deleteBookById(Integer id) {
        String sql="delete from `t_book` where `id` = ?";
        return baseDao.update(sql,id);
    }

    @Override
    public int updateBook(Book book) {
        String sql="update t_book set `name`=?,`price`=?,`author`=?,`sales`=?,`stock`=?,`img_path`=? where id = ?";

        return baseDao.update(sql,book.getName(),book.getPrice(),book.getAuthor(),book.getSales(),book.getStock(),book.getImgPath(),book.getId());

    }

    @Override
    public Book queryBookById(Integer id) {
        String sql="select `id`,`name`,`price`,`author`,`sales`,`stock`,`img_path` imgPath from t_book where id = ?";
        return baseDao.quertForOne(Book.class,sql,id);
    }

    @Override
    public List<Book> queryBooks() {
        String sql="select `id`,`name`,`price`,`author`,`sales`,`stock`,`img_path` imgPath from t_book";
        List<Book> books = baseDao.queryForList(Book.class, sql);
        return books;
    }

    @Override
    public List<Book> queryPages(int pageNo,int pageSize) {
        String sql= "select `id`,`name`,`price`,`author`,`sales`,`stock`,`img_path` imgPath from t_book limit ?,?";
        List<Book> books = baseDao.queryForList(Book.class, sql, pageNo, pageSize);
        return books;

    }
    public Object queryBookCounts()
    {
        String sql="select count(*) from t_book";
        Object o = baseDao.queryForValue(sql);
        return o;
    }

    @Override
    public Object queryBookCountsByPrice(int max, int min) {
        String sql="select count(*) from t_book where `price`>=? and `price`<=?";
        Object o = baseDao.queryForValue(sql, min, max);
        return o;
    }
}
