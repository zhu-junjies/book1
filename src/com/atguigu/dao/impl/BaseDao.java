package com.atguigu.dao.impl;

import com.atguigu.utils.JdbcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class BaseDao {
    private QueryRunner queryRunner=new QueryRunner();
    public int update(String sql,Object ... args)
    {
        Connection connection= JdbcUtils.getConnection();
        try {
            return queryRunner.update(connection,sql,args);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            JdbcUtils.close(connection);
        }
        return -1;
    }

    /**
     *
     * @param type ytt
     * @param sql
     * @param args
     * @param <T>
     * @return
     */
    public <T> T quertForOne(Class<T> type,String sql,Object...args)
    {
        Connection connection=JdbcUtils.getConnection();
        try {
            T query = queryRunner.query(connection, sql, new BeanHandler<T>(type), args);
            return query;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            JdbcUtils.close(connection);
        }
        return null;
    }
    public <T> List<T> queryForList(Class<T> clasz,String sql,Object... args) {
        Connection Connection=JdbcUtils.getConnection();
        try {
            List<T> query = queryRunner.query(Connection, sql, new BeanListHandler<T>(clasz),args);
            return query;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(Connection);
        }
        return null;
    }
    public Object queryForValue(String sql,Object...args)
    {
        Connection connection=JdbcUtils.getConnection();
        try {
            return queryRunner.query(connection,sql,new ScalarHandler(),args);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            JdbcUtils.close(connection);
        }
        return null;
    }
}