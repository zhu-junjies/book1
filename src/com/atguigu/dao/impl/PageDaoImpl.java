package com.atguigu.dao.impl;

import com.atguigu.dao.pageDao;

import java.util.List;

public class PageDaoImpl implements pageDao {
    @Override
    public <T> List<T> getItemPage(Class<T> clazz,String tableName, int start, int pageSize) {
        BaseDao baseDao = new BaseDao();
        String sql="SELECT * FROM "+tableName+" LIMIT ?,?";
        List<T> ts = baseDao.queryForList(clazz, sql, start, pageSize);
        return ts;
    }
    public <T> List<T> getItemByPricePage(Class<T> clazz,String tableName, int start, int pageSize,int max,int min) {
        BaseDao baseDao = new BaseDao();
        String sql="SELECT * FROM "+tableName+" WHERE `price`>=? AND `price`<=? order by price LIMIT ?,?";
        List<T> ts = baseDao.queryForList(clazz, sql, min, max,start,pageSize);
        return ts;
    }

}
