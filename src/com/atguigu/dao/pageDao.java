package com.atguigu.dao;

import com.atguigu.pojo.Page;

import java.util.List;

public interface pageDao {
    public <T> List<T> getItemPage(Class<T> clazz, String tableName, int start, int pageNo);
    public <T> List<T> getItemByPricePage(Class<T> clazz, String tableName, int start, int pageNo,int max,int min);
}
