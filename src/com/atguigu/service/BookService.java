package com.atguigu.service;

import com.atguigu.pojo.Book;
import com.atguigu.pojo.Page;

import java.util.List;

public interface BookService {
    public void addBook(Book book);
    public void deleteBookById(Integer id);
    public void updateBook(Book book);
    public Book queryBookById(Integer id);
    public List<Book> queryBooks();
    public List<Book> queryPage(int pageNo,int pageSize);
    public long queryBookCounts();
    public Page getBookPage(int pageSize,int pageNo);
    public Page getBookByPricePage(int pageSize,int pageNo,int max,int min);
    public long queryBookCountsByPrice(int max,int min);
}
