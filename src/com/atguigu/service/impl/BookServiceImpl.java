package com.atguigu.service.impl;

import com.atguigu.dao.BookDao;
import com.atguigu.dao.impl.BookDaoImpl;
import com.atguigu.dao.impl.PageDaoImpl;
import com.atguigu.dao.pageDao;
import com.atguigu.pojo.Book;
import com.atguigu.pojo.Page;
import com.atguigu.service.BookService;
import java.util.List;
public class BookServiceImpl implements BookService {
    BookDao bookDao = new BookDaoImpl();
    @Override
    public void addBook(Book book) {
    bookDao.addBook(book);
    }

    @Override
    public void deleteBookById(Integer id) {
    bookDao.deleteBookById(id);
    }

    @Override
    public void updateBook(Book book) {
    bookDao.updateBook(book);
    }

    @Override
    public Book queryBookById(Integer id) {
    return bookDao.queryBookById(id);
    }

    @Override
    public List<Book> queryBooks() {
        return bookDao.queryBooks();
    }

    @Override
    public List<Book> queryPage(int pageNo,int pageSize) {
        return bookDao.queryPages(pageNo,pageSize);
    }

    @Override
    public long queryBookCounts() {
        long a=(long)bookDao.queryBookCounts();
        return a;
    }

    @Override
    public Page getBookPage(int pageSize, int pageNo) {
        pageDao pageDao = new PageDaoImpl();
        Page<Book> bookPage = new Page<>();
        bookPage.setCount(this.queryBookCounts());
        bookPage.setStart((pageNo-1)*pageSize);
        bookPage.setPageSize(pageSize);
        bookPage.setPageNo(pageNo);
        bookPage.setPageCount(bookPage.getCount()%bookPage.getPageSize()==0?bookPage.getCount()/bookPage.getPageSize():bookPage.getCount()/bookPage.getPageSize()+1);
        if (pageNo>bookPage.getPageCount())
        {
            bookPage.setPageNo((int) bookPage.getPageCount());
        }
        if (pageNo<=0)
        {
            bookPage.setPageNo(1);
        }
        bookPage.setPageItem(pageDao.getItemPage(Book.class,"t_book",(bookPage.getPageNo()-1)*pageSize,pageSize));
        return bookPage;
    }

    @Override
    public Page getBookByPricePage(int pageSize, int pageNo, int max, int min) {
        pageDao pageDao = new PageDaoImpl();
        Page<Book> bookPage = new Page<>();
        bookPage.setCount(this.queryBookCountsByPrice(max,min));
        bookPage.setStart((pageNo-1)*pageSize);
        bookPage.setPageSize(pageSize);
        bookPage.setPageNo(pageNo);
        bookPage.setPageCount(bookPage.getCount()%bookPage.getPageSize()==0?bookPage.getCount()/bookPage.getPageSize():bookPage.getCount()/bookPage.getPageSize()+1);
        if (pageNo>bookPage.getPageCount())
        {
            bookPage.setPageNo((int) bookPage.getPageCount());
        }
        if (pageNo<=0)
        {
            bookPage.setPageNo(1);
        }
        bookPage.setPageItem(pageDao.getItemByPricePage(Book.class,"t_book",(bookPage.getPageNo()-1)*pageSize,pageSize,max,min));
        return bookPage;
    }

    @Override
    public long queryBookCountsByPrice(int max, int min) {
        return (long) bookDao.queryBookCountsByPrice(max,min);
    }
}
