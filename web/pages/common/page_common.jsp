<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: win10
  Date: 2021/4/12
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="page_nav">
    <a href="${requestScope.page.url}&pageNo=1&pageSize=${requestScope.page.pageSize}">首页</a>
    <c:if test="${requestScope.page.pageNo>1}">
        <a href="${requestScope.page.url}&pageNo=${requestScope.page.pageNo-1}&pageSize=${requestScope.page.pageSize}">上一页</a>
    </c:if>
    <c:choose>
        <c:when test="${requestScope.page.pageCount<=5}">
            <c:forEach begin="1" end="${requestScope.page.pageCount}" var="i">
                <c:if test="${requestScope.page.pageNo==i}">
                    <a>【${i}】</a>
                </c:if>
                <c:if test="${requestScope.page.pageNo!=i}">
                    <a href="${requestScope.page.url}&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a></c:if>
            </c:forEach>
        </c:when>
        <c:when test="${requestScope.page.pageCount>5}">
            <c:choose>
                <c:when test="${requestScope.page.pageNo<=3}">
                    <c:forEach begin="1" end="5" var="i">
                        <c:if test="${requestScope.page.pageNo==i}">
                            <a>【${i}】</a>
                        </c:if>
                        <c:if test="${requestScope.page.pageNo!=i}">
                            <a href="${requestScope.page.url}&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                        </c:if>
                    </c:forEach>
                </c:when>
                <c:when test="${requestScope.page.pageNo>3&&requestScope.page.pageNo<=requestScope.page.pageCount-2}">
                    <c:forEach begin="${requestScope.page.pageNo-2}" end="${requestScope.page.pageNo+2}" var="i">
                        <c:if test="${requestScope.page.pageNo==i}">
                            <a>【${i}】</a>
                        </c:if>
                        <c:if test="${requestScope.page.pageNo!=i}">
                            <a href="${requestScope.page.url}&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                        </c:if>
                    </c:forEach>
                </c:when>
                <c:when test="${requestScope.page.pageNo>requestScope.page.pageCount-2}">
                    <c:forEach begin="${requestScope.page.pageCount-4}" end="${requestScope.page.pageCount}" var="i">
                        <c:if test="${requestScope.page.pageNo==i}">
                            <a>【${i}】</a>
                        </c:if>
                        <c:if test="${requestScope.page.pageNo!=i}">
                            <a href="${requestScope.page.url}&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                        </c:if>
                    </c:forEach>
                </c:when>
            </c:choose>
        </c:when>
    </c:choose>
    <c:if test="${requestScope.page.pageNo*requestScope.page.pageSize<=requestScope.page.count-1}">
        <a href="${requestScope.page.url}&pageNo=${requestScope.page.pageNo+1}&pageSize=${requestScope.page.pageSize}">下一页</a>
    </c:if>
    <a href="${requestScope.page.url}&pageSize=${requestScope.page.pageSize}&pageNo=${requestScope.page.pageCount}">末页</a>
    共${requestScope.page.pageCount}页，${requestScope.page.count}条记录 到第<input name="pageNo" id="pn_input"/>页
    <input name="pageSize" type="hidden" value="${requestScope.page.pageSize}">
    <input type="submit" value="确定">
    <input type="hidden" name="action" value="page">

</div>
