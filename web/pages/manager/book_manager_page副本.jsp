<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>图书管理</title>
    <%@include file="../common/label_common.jsp"%>
</head>
<body>

<div id="header">
    <img class="logo_img" alt="" src="static/img/logo.gif">
    <span class="wel_word">图书管理系统</span>
    <%@include file="../common/manager_menu.jsp"%>
</div>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
%>

<div id="main">
    <table style="margin-top: 20px">
        <tr>
            <td>名称</td>
            <td>价格</td>
            <td>作者</td>
            <td>销量</td>
            <td>库存</td>
            <td colspan="2">操作</td>
        </tr>
        <c:forEach items="${requestScope.page.pageItem}" var="book">
            <tr>
                <td name="name">${book.name}</td>
                <td name="price">${book.price}</td>
                <td name="author">${book.author}</td>
                <td name="sales">${book.sales}</td>
                <td name="stock">${book.stock}</td>
                <td><a href="manager/bookServlet?id=${book.id}&action=getBook&&pageNo=${requestScope.page.pageNo}&pageSize=${requestScope.page.pageSize}">修改</a></td>
                <td><a href="manager/bookServlet?action=delete&id=${book.id}&pageNo=${requestScope.page.pageNo}&pageSize=${requestScope.page.pageSize}">删除</a></td>
            </tr>
        </c:forEach>
        <form action="manager/bookServlet?">
        <div id="page_nav">
            <a href="manager/bookServlet?action=page&pageNo=1&pageSize=${requestScope.page.pageSize}">首页</a>
            <c:if test="${requestScope.page.pageNo>1}">
                <a href="manager/bookServlet?action=page&pageNo=${requestScope.page.pageNo-1}&pageSize=${requestScope.page.pageSize}">上一页</a>
            </c:if>
            <c:choose>
                <c:when test="${requestScope.page.pageCount<=5}">
                    <c:forEach begin="1" end="${requestScope.page.pageCount}" var="i">
                        <c:if test="${requestScope.page.pageNo==i}">
                            <a>【${i}】</a>
                        </c:if>
                        <c:if test="${requestScope.page.pageNo!=i}">
                        <a href="manager/bookServlet?action=page&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a></c:if>
                    </c:forEach>
                </c:when>
                <c:when test="${requestScope.page.pageCount>5}">
                    <c:choose>
                        <c:when test="${requestScope.page.pageNo<=3}">
                            <c:forEach begin="1" end="5" var="i">
                                <c:if test="${requestScope.page.pageNo==i}">
                                    <a>【${i}】</a>
                                </c:if>
                                <c:if test="${requestScope.page.pageNo!=i}">
                                    <a href="manager/bookServlet?action=page&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                                </c:if>
                            </c:forEach>
                        </c:when>
                        <c:when test="${requestScope.page.pageNo>3&&requestScope.page.pageNo<=requestScope.page.pageCount-2}">
                            <c:forEach begin="${requestScope.page.pageNo-2}" end="${requestScope.page.pageNo+2}" var="i">
                                <c:if test="${requestScope.page.pageNo==i}">
                                    <a>【${i}】</a>
                                </c:if>
                                <c:if test="${requestScope.page.pageNo!=i}">
                                    <a href="manager/bookServlet?action=page&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                                </c:if>
                            </c:forEach>
                        </c:when>
                        <c:when test="${requestScope.page.pageNo>requestScope.page.pageCount-2}">
                            <c:forEach begin="${requestScope.page.pageCount-4}" end="${requestScope.page.pageCount}" var="i">
                                <c:if test="${requestScope.page.pageNo==i}">
                                    <a>【${i}】</a>
                                </c:if>
                                <c:if test="${requestScope.page.pageNo!=i}">
                                    <a href="manager/bookServlet?action=page&pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                                </c:if>
                            </c:forEach>
                        </c:when>
                    </c:choose>
                </c:when>
            </c:choose>
            <c:if test="${requestScope.page.pageNo*requestScope.page.pageSize<=requestScope.page.count-1}">
            <a href="manager/bookServlet?action=page&pageNo=${requestScope.page.pageNo+1}&pageSize=${requestScope.page.pageSize}">下一页</a>
            </c:if>
            <a href="manager/bookServlet?action=page&pageSize=${requestScope.page.pageSize}&pageNo=${requestScope.page.pageCount}">末页</a>
                共${requestScope.page.pageCount}页，${requestScope.page.count}条记录 到第<input name="pageNo" id="pn_input"/>页
                <input name="pageSize" type="hidden" value="${requestScope.page.pageSize}">
                <input type="submit" value="确定">
                <input type="hidden" name="action" value="page">
        </div>
        </form>
    </table>
</div>
<a href="pages/manager/book_edit.jsp?action=addBook&pageNo=${requestScope.page.pageNo}&pageSize=${requestScope.page.pageSize}" style="color:blue;margin-left: 49%;font-size: 15px">添加图书</a>
<div id="bottom">
		<span>
			尚硅谷书城.Copyright &copy;2015
		</span>
</div>
</body>
</html>