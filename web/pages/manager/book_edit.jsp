<%@ page import="com.atguigu.pojo.Book" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <%@include file="../common/label_common.jsp"%>
    <title>编辑图书</title>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-top: 200px;
        }

        h1 a {
            color: red;
        }

        input {
            text-align: center;
        }
    </style>
</head>
<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
%>
<div id="header">
    <img class="logo_img" alt="" src="static/img/logo.gif">
    <span class="wel_word">编辑图书</span>
    <%@include file="../common/manager_menu.jsp"%>
</div>
<div id="main">
    <form action="manager/bookServlet">
        <table>
            <tr>
                <td>名称</td>
                <td>价格</td>
                <td>作者</td>
                <td>销量</td>
                <td>库存</td>
                <td colspan="2">操作</td>
            </tr>
            <tr>
                <%
                    Object book = request.getAttribute("book");
                    Object pageNo = request.getAttribute("pageNo");
                    Object pageSize = request.getAttribute("pageSize");
                %>
                <td><input name="name" type="text" value="${book.name}"/></td>
                <td><input name="price" type="text" value="${book.price}"/></td>
                <td><input name="author" type="text" value="${book.author}"/></td>
                <td><input name="sales" type="text" value="${book.sales}"/></td>
                <td><input name="stock" type="text" value="${book.stock}"/></td>
                <c:if test="${param.action=='addBook'}">
                    <input type="hidden" name="action" value="addBook">
                    <input type="hidden" name="pageNo" value=${param.pageNo}>
                    <input type="hidden" name="pageSize" value=${param.pageSize}>
                    <td><input type="submit" value="添加"/></td>
                </c:if>
                <c:if test="${param.action=='getBook'}">
                    <input type="hidden" name="id" value="${book.id}">
                    <input type="hidden" name="action" value="update">
                    <input type="hidden" name="pageNo" value=${pageNo}>
                    <input type="hidden" name="pageSize" value=${pageSize}>
                    <td><input type="submit" value="提交"/></td>
                </c:if>
            </tr>
        </table>
    </form>


</div>

<div id="bottom">
			<span>
				尚硅谷书城.Copyright &copy;2015
			</span>
</div>
</body>
</html>