<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>书城首页</title>
    <%@include file="/pages/common/label_common.jsp"%>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="static/img/logo.gif">
    <span class="wel_word">网上书城</span>
    <div>
        <a href="pages/user/login.jsp">登录</a> |
        <a href="pages/user/regist.jsp">注册</a> &nbsp;&nbsp;
        <a href="pages/cart/cart.jsp">购物车</a>
        <a href="pages/manager/manager.jsp">后台管理</a>
    </div>
</div>
<div id="main">
    <div id="book">
        <div class="book_cond">
            <form action="" method="get">
                价格：<input id="min" type="text" name="min" value=""> 元 -
                <input id="max" type="text" name="max" value=""> 元
                <input type="submit" value="查询"/>
            </form>
        </div>
        <div style="text-align: center">
            <span>您的购物车中有3件商品</span>
            <div>
                您刚刚将<span style="color: red">时间简史</span>加入到了购物车中
            </div>
        </div>
        <c:forEach items="${requestScope.page.pageItem}" var="s1">
        <div class="b_list">
            <div class="img_div">
                <img class="book_img" alt="" src="${s1.imgPath}"/>
            </div>
            <div class="book_info">
                <div class="book_name">
                    <span class="sp1">书名:</span>
                    <span class="sp2">${s1.name}</span>
                </div>
                <div class="book_author">
                    <span class="sp1">作者:</span>
                    <span class="sp2">${s1.author}</span>
                </div>
                <div class="book_price">
                    <span class="sp1">价格:</span>
                    <span class="sp2">${s1.price}</span>
                </div>
                <div class="book_sales">
                    <span class="sp1">销量:</span>
                    <span class="sp2">${s1.sales}</span>
                </div>
                <div class="book_amount">
                    <span class="sp1">库存:</span>
                    <span class="sp2">${s1.stock}</span>
                </div>
                <div class="book_add">
                    <button>加入购物车</button>
                </div>
            </div>
        </div>
        </c:forEach>
        <form action="client/clientServlet">
    <div id="page_nav">
        <a href="client/clientServlet?pageNo=1">首页</a>
        <c:if test="${requestScope.page.pageNo>1}">
            <a href="client/clientServlet?pageNo=${requestScope.page.pageNo-1}&pageSize=${requestScope.page.pageSize}">上一页</a>
        </c:if>
        <c:choose>
            <c:when test="${requestScope.page.pageCount<=5}">
                <c:forEach begin="1" end="${requestScope.page.pageCount}" var="i">
                    <c:if test="${requestScope.page.pageNo==i}">
                        <a>【${i}】</a>
                    </c:if>
                    <c:if test="${requestScope.page.pageNo!=i}">
                        <a href="client/clientServlet?pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a></c:if>
                </c:forEach>
            </c:when>
            <c:when test="${requestScope.page.pageCount>5}">
                <c:choose>
                    <c:when test="${requestScope.page.pageNo<=3}">
                        <c:forEach begin="1" end="5" var="i">
                            <c:if test="${requestScope.page.pageNo==i}">
                                <a>【${i}】</a>
                            </c:if>
                            <c:if test="${requestScope.page.pageNo!=i}">
                                <a href="client/clientServlet?pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                            </c:if>
                        </c:forEach>
                    </c:when>
                    <c:when test="${requestScope.page.pageNo>3&&requestScope.page.pageNo<=requestScope.page.pageCount-2}">
                        <c:forEach begin="${requestScope.page.pageNo-2}" end="${requestScope.page.pageNo+2}" var="i">
                            <c:if test="${requestScope.page.pageNo==i}">
                                <a>【${i}】</a>
                            </c:if>
                            <c:if test="${requestScope.page.pageNo!=i}">
                                <a href="client/clientServlet?pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                            </c:if>
                        </c:forEach>
                    </c:when>
                    <c:when test="${requestScope.page.pageNo>requestScope.page.pageCount-2}">
                        <c:forEach begin="${requestScope.page.pageCount-4}" end="${requestScope.page.pageCount}" var="i">
                            <c:if test="${requestScope.page.pageNo==i}">
                                <a>【${i}】</a>
                            </c:if>
                            <c:if test="${requestScope.page.pageNo!=i}">
                                <a href="client/clientServlet?pageNo=${i}&pageSize=${requestScope.page.pageSize}">${i}</a>
                            </c:if>
                        </c:forEach>
                    </c:when>
                </c:choose>
            </c:when>
        </c:choose>
        <c:if test="${requestScope.page.pageNo*requestScope.page.pageSize<=requestScope.page.count-1}">
            <a href="client/clientServlet?pageNo=${requestScope.page.pageNo+1}&pageSize=${requestScope.page.pageSize}">下一页</a>
        </c:if>
        <a href="client/clientServlet?pageSize=${requestScope.page.pageSize}&pageNo=${requestScope.page.pageCount}">末页</a>
        共${requestScope.page.pageCount}页，${requestScope.page.count}条记录 到第<input name="pageNo" id="pn_input"/>页
        <input name="pageSize" type="hidden" value="${requestScope.page.pageSize}">
        <input type="submit" value="确定">
        </form>
    </div>

</div>

<div id="bottom">
		<span>
			${pageContext.request.serverName}:${pageContext.request.serverPort}尚硅谷书城.Copyright &copy;2015
		</span>
</div>
</body>
</html>