<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>购物车</title>
    <%@include file="../common/label_common.jsp"%>
    <script type="text/javascript">
        $(function () {
            $("a.deleteItem").click(function () {
                var jQuery = $(this).parent().parent().find("td:eq(0)").text();
                confirm("确定要删除"+jQuery);
            })
            $("a.clear").click(function () {
            confirm("确定要清空购物车？？")
            })
            $(".updateCount").change(function () {
                var jQuery = $(this).parent().parent().find("td:eq(0)").text();
                var value = this.value;
                var attr = $(this).attr("bookId");
                if(confirm("你确定要修改"+jQuery+"的数量为"+value+"？"))
                {
                location.href="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/cartServlet?action=updateCount&count="+value+"&bookId="+attr;
                }else {
                    this.value=this.defaultValue;
                }
            })
        })
    </script>
</head>
<body>

<div id="header">
    <img class="logo_img" alt="" src="static/img/logo.gif">
    <span class="wel_word">购物车</span>
    <div>
        <%@ include file="../common/login_success_common.jsp"%>
    </div>
</div>
<div id="main">
    <table>
        <tr>
            <td>商品名称</td>
            <td>数量</td>
            <td>单价</td>
            <td>金额</td>
            <td>操作</td>
        </tr>
        <c:if test="${empty sessionScope.cart.items}">
            <tr>
                <td colspan="5"><a href="index.jsp">你当前购物车没有任何商品,快给老子去买东西</a></td>
            </tr>
        </c:if>
    <c:if test="${not empty sessionScope.cart.items}">
        <c:forEach items="${sessionScope.cart.items.values()}" var="s1">
            <tr>
                <td>${s1.name}</td>
                <td bookId = "${s1.id}"><input value="${s1.count}"
                                               bookId="${s1.id}"
                                               class="updateCount"></td>
                <td>${s1.price}</td>
                <td>${s1.totalPrice}</td>
                <td><a href="cartServlet?action=deleteItem&bookId=${s1.id}" class="deleteItem">删除</a></td>
            </tr>
        </c:forEach>
    </table>
        <div class="cart_info">
            <span class="cart_span">购物车中共有<span class="b_count">${sessionScope.cart.totalCount}</span>件商品</span>
            <span class="cart_span">总金额<span class="b_price">${sessionScope.cart.totalPrice}</span>元</span>
            <span class="cart_span"><a href="cartServlet?action=clear" class="clear">清空购物车</a></span>
            <span class="cart_span"><a href="pages/cart/checkout.jsp">去结账</a></span>
        </div>
    </c:if>
    <div id="bottom">
		<span>
			尚硅谷书城.Copyright &copy;2015
		</span>
    </div>

</div>
</body>
</html>